package com.kun.lazzy.balok;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class ResultActivity extends AppCompatActivity{
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil);

        Intent intent = getIntent();

        String result = intent.getStringExtra("data");
        String nama = intent.getStringExtra("data2");
        TextView tv = findViewById(R.id.tv_result);
        TextView tv2 = findViewById(R.id.tv_result2);
        tv2.setText(nama);
        tv.setText(result);
    }
}

