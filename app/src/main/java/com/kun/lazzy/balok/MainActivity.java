package com.kun.lazzy.balok;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private EditText et_lebar, et_tinggi, et_panjang;
    private double p, l, t;
    Button hitung_keliling, hitung_luas, hitung_volume;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        et_panjang = findViewById(R.id.et_panjang);
        et_lebar = findViewById(R.id.et_lebar);
        et_tinggi = findViewById(R.id.et_tinggi);
    }

    private void data(List<String> variabel){
        String panjang = et_panjang.getText().toString();
        String lebar = et_lebar.getText().toString();
        String tinggi = et_tinggi.getText().toString();
        p = Double.parseDouble(panjang);
        l = Double.parseDouble(lebar);
        t = Double.parseDouble(tinggi);
    }


    public void hitung_keliling(View view) {
        if(et_panjang.getText().toString().isEmpty()&& et_lebar.getText().toString().isEmpty()&&et_tinggi.getText().toString().isEmpty()){
            et_panjang.setError("Harap isi dahhulu");
            et_lebar.setError("Harap isi dahhulu");
            et_tinggi.setError("Harap isi dahhulu");
            return;
        }


        List<String> variabel = new ArrayList<>();
        data(variabel);

        Double keliling = 4*(p + t + l);
        String nama = "Keliling";

        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra("data", String.valueOf(keliling));
        intent.putExtra("data2", String.valueOf(nama));
        startActivity(intent);
    }

    public void hitung_luas(View view) {
       if(et_panjang.getText().toString().isEmpty()&& et_lebar.getText().toString().isEmpty()&&et_tinggi.getText().toString().isEmpty()){
           et_panjang.setError("Harap isi dahhulu");
           et_lebar.setError("Harap isi dahhulu");
           et_tinggi.setError("Harap isi dahhulu");
           return;
       }
        List<String> variabel = new ArrayList<>();
        data(variabel);

        Double pl = p*l;
        Double pt = p*t;
        Double lt = l*t;

        String nama = "Luas";

        Double luas = 2*(pl + pt + lt);
        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra("data", String.valueOf(luas));
        intent.putExtra("data2", String.valueOf(nama));
        startActivity(intent);

    }

    public void hitung_volume(View view) {
        if(et_panjang.getText().toString().isEmpty()&& et_lebar.getText().toString().isEmpty()&&et_tinggi.getText().toString().isEmpty()){
            et_panjang.setError("Harap isi dahhulu");
            et_lebar.setError("Harap isi dahhulu");
            et_tinggi.setError("Harap isi dahhulu");
            return;
        }

        List<String> variabel = new ArrayList<>();
        data(variabel);

        Double voulume = p * l * t;
        String nama = "Volume";


        Intent intent = new Intent(this, ResultActivity.class);
        intent.putExtra("data2", String.valueOf(nama));
        intent.putExtra("data", String.valueOf(voulume));
        startActivity(intent);
    }
}

